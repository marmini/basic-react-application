import React from "react";
import { Carousel } from "antd";

import LoginComponent from "./LoginComponent";
import Carousel1 from "../../images/Carousel1.jpg";
import Carousel2 from "../../images/Carousel2.jpg";

import { LoginPageBackground, LoginBox, LoginSection, Login } from "./styles";

const LoginPage = () => {
  const contentStyle = {
    height: "740px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };
  document.title = "Login";

  let carouselList = [
    {
      imageSrc: Carousel1,
      height: "100%",
      width: "100%",
    },
    {
      imageSrc: Carousel2,
      height: "100%",
      width: "100%",
    },
  ];
  return (
    <Login>
      <LoginSection>
        <LoginPageBackground>
          <Carousel autoplay>
            {carouselList &&
              Array.isArray(carouselList) &&
              carouselList.map(({ imageSrc, height, width }) => {
                return (
                  <div>
                    <h3 style={contentStyle}>
                      <img
                        src={imageSrc}
                        alt="carousel"
                        style={{ height, width }}
                      />
                    </h3>
                  </div>
                );
              })}
          </Carousel>
        </LoginPageBackground>
        <LoginBox>
          <LoginComponent />
        </LoginBox>
      </LoginSection>
    </Login>
  );
};

export { LoginPage };
