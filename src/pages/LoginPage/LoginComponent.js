import React from "react";
import { Form, Input, message, Icon } from "antd";

import history from "../../common/history";
import { Link } from "react-router-dom";
import { HeaderLogo, Welcome, Add, LoginButton } from "./styles";

import "./Login.css";

const FormItem = Form.Item;

class LoginComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userToken: props.userToken || "",
    };
  }

  componentWillReceiveProps({ userToken }) {
    this.setState({ userToken });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        history.push("/homepage");
      } else {
        message.error("Please enter valid username and password");
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <section style={{ width: "100%" }}>
        <HeaderLogo>Login</HeaderLogo>

        <Welcome>
          Don't have an account yet?{" "}
          <Link
            style={{ color: "black", fontWeight: "bold" }}
            to={{ pathname: `/signup` }}
          >
            Sign up
          </Link>{" "}
        </Welcome>

        <Form
          onSubmit={this.handleSubmit}
          className="form-v1"
          style={{ textAlign: "center" }}
        >
          <FormItem>
            <LoginButton
              style={{
                backgroundColor: "#0066b8",
                borderColor: "#0066b8",
                width: "380px",
                textAlign: "center",
              }}
              type="primary"
            >
              <Icon type="facebook" theme="filled" />
              Login via facebook
            </LoginButton>
          </FormItem>
          <FormItem>
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  required: true,
                  message: "Please input your email!",
                },
              ],
            })(
              <Input
                size="medium"
                style={{ width: "380px" }}
                prefix={<Icon type="user" />}
                placeholder="Email Address"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Please input your Password!" },
              ],
            })(
              <Input
                size="medium"
                style={{ width: "380px" }}
                prefix={<Icon type="lock" />}
                type="password"
                placeholder="Password"
              />
            )}
          </FormItem>
          <FormItem>
            <LoginButton
              style={{
                backgroundColor: "#17c417",
                borderColor: "#0066b8",
                width: "380px",
              }}
              type="primary"
              htmlType="submit"
            >
              Login our community
            </LoginButton>
          </FormItem>
        </Form>

        <Add>
          By joining you agree to the <b>Terms</b> and <b>Privacy Policy</b>
        </Add>
      </section>
    );
  }
}

const LoginComponentForm = Form.create()(LoginComponent);
export default LoginComponentForm;
