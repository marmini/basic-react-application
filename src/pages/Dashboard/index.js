import React from "react";
import { Card, Icon } from "antd";

const { Meta } = Card;
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let employeeMaster = [
      {
        image: "https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png",
        email: "marmini@gmail.com",
        phone: "9133576677",
      },
    ];
    return (
      <div>
        {employeeMaster &&
          Array.isArray(employeeMaster) &&
          employeeMaster.map(({ image, email, phone }) => {
            return (
              <Card
                hoverable
                style={{ width: 300 }}
                cover={
                  <img
                    alt="images"
                    src={image}
                    style={{ height: "300px", width: "300px" }}
                  />
                }
              >
                <Meta
                  description={
                    <div>
                      <Icon type="mail"></Icon> {email}
                    </div>
                  }
                />
                <Meta
                  description={
                    <div>
                      <Icon type="phone"></Icon> {phone}
                    </div>
                  }
                />
              </Card>
            );
          })}
      </div>
    );
  }
}

export default Dashboard;
