import React, { Component } from "react";
import { Router, Route, Redirect } from "react-router-dom";

import history from "../common/history";
import { LoginPage } from "../pages/LoginPage/LoginPage";
import DashboardPage from "../pages/Dashboard";
import Homepage from "../pages/HomePage";

export default class Routerr extends Component {
  render() {
    return (
      <Router history={history}>
        <div>
          <Route exact path="/" render={() => <Redirect to="/login" />} />
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/homepage" component={Homepage} />
          <Route exact path="/dashboard" component={DashboardPage} />
        </div>
      </Router>
    );
  }
}
